jQuery(function($) {
    function processIncludes(root) {
        $(root).find("[data-include]").each(function(i, el) {
            var $el = $(el);
            $el.load($el.data("include"), function() {
                processIncludes(el);
            });
        });
    }

    processIncludes(document.body);
});
